from datetime import datetime

from pydantic import BaseModel


class SBaseModel(BaseModel):
    createdAt: datetime = None
    updatedAt: datetime = None
    isDeleted: bool = None
    deletedAt: datetime = None

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {
            datetime: datetime.isoformat,
        }
