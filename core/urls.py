# -*- coding: utf-8 -*-

from fastapi.routing import APIRouter

from apps.healthcheck.controllers.healthcheck_controller import router as router_health_check
from apps.auth.controllers.auth_controller import router as router_auth
from apps.wallet.controllers.wallet_controller import router as router_wallet
from apps.featureconfig.controllers.featureconfig_controller import router as router_feature
from apps.sentry.controllers.sentry_controller import router as router_sentry
from apps.appclient.controllers.appclient_controller import router as router_client

router = APIRouter()
router.include_router(router_health_check)
router.include_router(router_auth)
router.include_router(router_wallet)
router.include_router(router_feature)
router.include_router(router_sentry)
router.include_router(router_client)
