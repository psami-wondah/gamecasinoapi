import math
from datetime import datetime
from typing import Any, List, Tuple, TypeVar, Type

from bson import ObjectId
from pymongo import DESCENDING
from pymongo.client_session import ClientSession
from pymongo.collection import Collection
from pymongo.results import InsertOneResult, InsertManyResult, UpdateResult

import re

from core.utils.response_service import MetaDataModel


class ModelUtilityService:
    T = TypeVar('T')

    @staticmethod
    def __pluralize(noun: str) -> str:
        if re.search('[xz]$', noun):
            return re.sub('$', 'es', noun)
        elif re.search('[s]$', noun):
            return re.sub('$', '', noun)
        elif re.search('[y]$', noun):
            return re.sub('$', '', noun)
        elif re.search('[^aeioudgkprt]h$', noun):
            return re.sub('$', 'es', noun)
        elif re.search('[aeiou]$', noun):
            return re.sub('$', 'ies', noun)
        else:
            return noun + 's'

    @staticmethod
    def paginate_data(model: Collection, query: dict, page_num: int, page_size: int, genericClassType: Type[T]) -> \
            Tuple[List[T], MetaDataModel]:

        """returns a set of documents belonging to page number `page_num`
                where size of each page is `page_size`.
                """
        # Calculate number of documents to skip
        skips = page_size * (page_num - 1)

        total_count = model.find(query).count()

        total_page = math.ceil(total_count / page_size)

        prev_page = (lambda: page_num - 1 if (page_num - 1) > 0 else None)()
        next_page = (lambda: page_num + 1 if (page_num + 1)
                     <= total_page else None)()

        # Skip and limit
        documents = model.find(query).sort(
            'createdAt', DESCENDING).skip(skips).limit(page_size)

        result = list(documents)
        result_count = len(result)

        meta_data = {
            'page': page_num, 'perPage': page_size,
            'total': total_count, 'pageCount': result_count,
            'previousPage': prev_page, 'nextPage': next_page
        }

        # Return documents
        return result, MetaDataModel(**meta_data)

    @staticmethod
    def model_populate(model: Collection, query: dict, fields: List[str], page_num: int, page_size: int,
                       genericClassType: Type[T]) -> Tuple[List[T], MetaDataModel]:

        # Calculate number of documents to skip
        skips = page_size * (page_num - 1)

        pipeline = [{
            '$facet': {
                'pipelineData': [
                    {
                        '$match': query,
                    },
                    {
                        '$sort': {'createdAt': DESCENDING}
                    },
                    {
                        '$skip': skips
                    },
                    {
                        '$limit': page_size
                    },
                ],
                'pipelineCount': [
                    {
                        '$match': query,
                    },
                    {
                        '$skip': skips
                    },
                    {
                        '$limit': page_size
                    },
                    {
                        '$count': 'pageCount'
                    }
                ],
                'totalCount': [
                    {
                        '$match': query,
                    },
                    {
                        '$count': 'count'
                    }
                ]
            }
        }]
        for field in fields:
            lookup = {
                '$lookup': {
                    'from': ModelUtilityService.__pluralize(field),
                    'localField': field,
                    'foreignField': '_id',
                    'as': field
                }
            }
            unwind = {
                '$unwind': {
                    'path': f'${field}'
                }
            }
            pipeline[0]['$facet']['pipelineData'] += [lookup, unwind]

        aggregation_result = list(model.aggregate(pipeline))

        result = aggregation_result[0]['pipelineData']
        pipeline_count = aggregation_result[0]['pipelineCount']
        pipeline_total_count = aggregation_result[0]['totalCount']

        result_count = pipeline_count[0]['pageCount'] \
            if len(pipeline_count) > 0 else 0
        total_count = pipeline_total_count[0]['count'] \
            if len(pipeline_total_count) > 0 else 0

        total_page = math.ceil(total_count / page_size)

        prev_page = (lambda: page_num - 1 if (page_num - 1) > 0 else None)()
        next_page = (lambda: page_num + 1 if (page_num + 1)
                     <= total_page else None)()

        meta_data = {
            'page': page_num, 'perPage': page_size,
            'total': total_count, 'pageCount': result_count,
            'previousPage': prev_page, 'nextPage': next_page
        }

        # And there goes the populate function just as mongoose populate works 🚀🕺🏽
        return result, MetaDataModel(**meta_data)

    @staticmethod
    def find_one_and_populate(model: Collection, query: dict, fields: List[str], genericClassType: Type[T]) -> T:
        pipeline = [{
            '$match': query,
        }]
        for field in fields:
            lookup = {
                '$lookup': {
                    'from': ModelUtilityService.__pluralize(field),
                    'localField': field,
                    'foreignField': '_id',
                    'as': field
                }
            }
            unwind = {
                '$unwind': {
                    'path': f'${field}'
                }
            }
            pipeline += [lookup, unwind]

        limit = {
            '$limit': 1
        }
        pipeline.append(limit)
        [aggregation_result] = list(model.aggregate(pipeline))
        return genericClassType(**aggregation_result)

    @staticmethod
    def find_and_populate(model: Collection, query: dict, fields: List[str], genericClassType: Type[T]) -> List[T]:
        pipeline = [{
            '$match': query,
        }]
        for field in fields:
            lookup = {
                '$lookup': {
                    'from': ModelUtilityService.__pluralize(field),
                    'localField': field,
                    'foreignField': '_id',
                    'as': field
                }
            }
            unwind = {
                '$unwind': {
                    'path': f'${field}'
                }
            }
            pipeline += [lookup, unwind]
        return list(model.aggregate(pipeline))

    @staticmethod
    def model_create(model: Collection, record: dict, genericClassType: Type[T], session: ClientSession = None) -> T:
        # Adding the SBaseModel attributes to the insertion
        record['createdAt'] = datetime.now()
        record['isDeleted'] = False
        record['updatedAt'] = datetime.now()

        created_record: InsertOneResult = model.insert_one(
            record, session=session)

        res = model.find_one(
            {'_id': ObjectId(created_record.inserted_id)}, session=session)

        return genericClassType(**res)

    @staticmethod
    def model_update(model: Collection, query: dict, record_to_update: dict) -> UpdateResult:
        record = {
            'updatedAt': datetime.now(),
            **record_to_update
        }
        updated_record: UpdateResult = model.update_one(
            query, {'$set': record})

        return updated_record

    @staticmethod
    def model_create_many(model: Collection, records: List[Any]) -> InsertManyResult:
        # Adding the SBaseModel attributes to the insertion
        for record in records:
            record['createdAt'] = datetime.now()
            record['isDeleted'] = False
            record['updatedAt'] = datetime.now()

        created_records: InsertManyResult = model.insert_many(records)

        return created_records
