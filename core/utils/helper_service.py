import binascii
import hashlib
import os
import random
import string

from itsdangerous import URLSafeTimedSerializer
from pydantic import EmailStr

from core.config import settings, SECRET_KEY
from core.utils.loggly import logger


class HelperService:

    @staticmethod
    def generate_random(length: int = 12, chars=string.ascii_letters + string.digits) -> str:
        return ''.join(random.choice(chars) for _ in range(length))

    @staticmethod
    def hash_password(password: str) -> str:
        """Hash a password for storing."""
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                      salt, 100000)
        pwdhash = binascii.hexlify(pwdhash)
        return (salt + pwdhash).decode('ascii')

    @staticmethod
    def verify_password(stored_password, provided_password):
        """Verify a stored password against one provided by user"""
        salt = stored_password[:64]
        stored_password = stored_password[64:]
        pwd_hash = hashlib.pbkdf2_hmac('sha512',
                                       provided_password.encode('utf-8'),
                                       salt.encode('ascii'),
                                       100000)
        pwd_hash = binascii.hexlify(pwd_hash).decode('ascii')
        return pwd_hash == stored_password

    @staticmethod
    def generate_confirmation_token(email: EmailStr) -> str:
        serializer = URLSafeTimedSerializer(SECRET_KEY)
        return serializer.dumps(email, salt=settings.SECURITY_PASSWORD_SALT)

    @staticmethod
    def confirm_token(token: str, expiration=settings.TOKEN_EXPIRATION_IN_SEC):
        serializer = URLSafeTimedSerializer(SECRET_KEY)
        try:
            email = serializer.loads(
                token,
                salt=settings.SECURITY_PASSWORD_SALT,
                max_age=expiration
            )
            return email
        except Exception as e:
            logger.error(e)
            return False
