from apscheduler.schedulers.asyncio import AsyncIOScheduler

from apps.notification.slack.services.slack_service import SlackService
from core.utils.loggly import logger

slackService = SlackService


def test_job():
    logger.info('CRON JOB BOT HERE')


class CronJob:
    scheduler = AsyncIOScheduler()

    test_job = scheduler.add_job(test_job, 'interval', seconds=100)
    notify_slack_for_demigod = scheduler.add_job(
        slackService.notify_slack_of_demigod, 'interval', seconds=10800)
