from typing import Any

from fastapi import HTTPException, status

from apps.featureconfig.interfaces.featureconfig_interface import FeatureConfig, FeatureName, FeatureStatusUpdateDTO
from apps.notification.slack.services.slack_service import SlackService
from core.db import db
from core.utils.model_utility_service import ModelUtilityService

modelUtilityService = ModelUtilityService
slackService = SlackService


class FeatureConfigService:

    @staticmethod
    def get_feature_by_name(name: FeatureName) -> FeatureConfig:
        feature_config = db.featureconfigs.find_one({
            'name': name
        })

        if not feature_config:
            feature_config = modelUtilityService.model_create(db.featureconfigs,
                                                              {'name': name, 'isEnabled': False}, FeatureConfig)
        else:
            feature_config = FeatureConfig(**feature_config)
        return feature_config

    @staticmethod
    def is_feature_enabled(name: FeatureName) -> bool:
        feature_config = FeatureConfigService.get_feature_by_name(name)
        return feature_config.isEnabled and not feature_config.isDeleted

    @staticmethod
    def check_feature_enabled_or_fail(name: FeatureName) -> Any:
        feature_enabled = FeatureConfigService.is_feature_enabled(name)
        if not feature_enabled:
            raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=f'{name} has been temporarily disabled')

    @staticmethod
    def update_feature(feature_name: FeatureName, feature_status_update_dto: FeatureStatusUpdateDTO) -> None:

        query = {'name': feature_name, 'isDeleted': False}
        record_to_update = {'isEnabled': feature_status_update_dto.status}

        modelUtilityService.model_update(db.featureconfigs, query, record_to_update)
        slackService.send_message(
            f'{feature_name} feature status has been updated to {feature_status_update_dto.status}',
            'backend')
