from enum import Enum
from typing import Optional

from bson import ObjectId
from pydantic.main import BaseModel
from pymongo import ASCENDING

from core.db import db
from core.depends.get_object_id import PyObjectId
from pydantic import Field

from core.depends.model import SBaseModel

from core.utils.loggly import logger


class FeatureName(str, Enum):
    SignUpBonus = 'sign-up-bonus'


class FeatureStatusUpdateDTO(BaseModel):
    status: bool


class FeatureConfigOut(BaseModel):
    id: Optional[PyObjectId]
    name: FeatureName
    isEnabled: bool

    def __init__(self, **pydict):
        try:
            super().__init__(**pydict)
            self.id = pydict.pop('_id')
        except Exception as e:
            logger.error(f'Error while mapping the id to record - {e}')

    class Config:
        arbitrary_types_allowed = True


class FeatureConfig(SBaseModel):
    __name__ = 'featureconfigs'
    id: Optional[PyObjectId] = Field(alias='_id')
    name: FeatureName
    isEnabled: bool = Field(default=False)

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        json_encoders = {
            ObjectId: str
        }

    @staticmethod
    def init():
        db.country.create_index(
            [('name', ASCENDING)], unique=True)
