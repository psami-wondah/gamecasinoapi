# -*- coding: utf-8 -*-
import json
from types import SimpleNamespace
from typing import Any

from fastapi.routing import APIRouter
from fastapi import status, Response

from core.utils.model_utility_service import ModelUtilityService
from core.utils.response_service import ResponseService

router = APIRouter(prefix='/api/v1/sentry', tags=['Sentry 🐞'])

modelUtilityService = ModelUtilityService


@router.post('/')
def webhook(res: Response, sentry_payload: Any):
    payload = json.loads(sentry_payload.json(), object_hook=lambda d: SimpleNamespace(**d))
    print(payload.action)
    return ResponseService.send_response(res, status.HTTP_200_OK, 'all good here')
