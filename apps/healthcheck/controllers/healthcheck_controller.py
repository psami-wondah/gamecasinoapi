# -*- coding: utf-8 -*-
from fastapi.routing import APIRouter
from fastapi import status, Response, Request

from apps.appclient.services.appclient_service import AppClientService

from core.utils.model_utility_service import ModelUtilityService
from core.utils.response_service import ResponseService

router = APIRouter(prefix='/api/v1/health-check', tags=['Health Check 🩺'])

modelUtilityService = ModelUtilityService
appClientService = AppClientService
clientAuth = appClientService.clientAuth


@router.post('/')
@clientAuth
def health_check(request: Request, res: Response):
    print(request.state.app_client)

    return ResponseService.send_response(res, status.HTTP_200_OK, 'all good here')
