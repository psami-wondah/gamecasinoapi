from typing import Optional

from bson import ObjectId
from pymongo import ASCENDING

from core.db import db
from core.depends.get_object_id import PyObjectId
from pydantic import Field

from core.depends.model import SBaseModel

from core.utils.loggly import logger


class CountryOut(SBaseModel):
    id: Optional[PyObjectId]
    name: str
    code: str
    callingCode: str
    flag: str

    def __init__(self, **pydict):
        try:
            super().__init__(**pydict)
            self.id = pydict.pop('_id')
        except Exception as e:
            logger.error(f'Error while mapping the id to record - {e}')

    class Config:
        arbitrary_types_allowed = True


class Country(SBaseModel):
    __name__ = 'country'
    id: Optional[PyObjectId] = Field(alias='_id')
    name: str
    code: str
    callingCode: str
    flag: str
    metaData: Optional[dict]

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        json_encoders = {
            ObjectId: str
        }

    @staticmethod
    def init():
        db.country.create_index(
            [('name', ASCENDING)], unique=True)
