from apps.country.interfaces.country_interface import CountryOut
from typing import List, Any

from core.db import db
from core.utils.model_utility_service import ModelUtilityService

modelUtilityService = ModelUtilityService


class CountryService:

    @staticmethod
    def get_country(page_num: int, page_size: int) -> tuple[List[CountryOut], Any]:
        query = {
            'isDeleted': False
        }
        res, meta_data = modelUtilityService.paginate_data(
            db.country, query, page_num, page_size, CountryOut)

        return res, meta_data

    @staticmethod
    def search_country(name: str, page_num: int, page_size: int) -> tuple[List[CountryOut], Any]:
        query = {
            'isDeleted': False,
            '$or': [
                {'name': {'$regex': name, '$options': 'i'}},
                {'code': {'$regex': name, '$options': 'i'}},
                {'callingCode': {'$regex': name, '$options': 'i'}}
            ]
        }
        res, meta_data = modelUtilityService.paginate_data(
            db.country, query, page_num, page_size, CountryOut)

        return res, meta_data
