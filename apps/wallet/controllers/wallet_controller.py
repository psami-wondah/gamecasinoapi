# -*- coding: utf-8 -*-
from apps.auth.services.auth_bearer import JWTBearer
from apps.user.interfaces.user_interface import User
from apps.wallet.interfaces.wallet_interface import WalletOut

from fastapi import status, Request, Depends, Response
from fastapi.routing import APIRouter

from apps.wallet.services.wallet_service import WalletService

from core.utils.response_service import ResponseService, get_response_model


walletService = WalletService
router = APIRouter(prefix='/api/v1/wallets',
                   tags=['Wallet 💸'])


@router.get('/retrieve-wallet', dependencies=[Depends(JWTBearer())],
            response_model=get_response_model(WalletOut, 'WalletResponse'))
async def retrieve_wallet(request: Request, response: Response):
    try:
        if hasattr(request.state, 'error'):
            raise request.state.error
        user: User = request.state.user
        request.app.logger.info(
            'checking wallet for - {user.id}')
        user_wallet = walletService.retrieve_wallet(user)
        request.app.logger.info('done retrieving wallet')
        return ResponseService.send_response(response, status.HTTP_200_OK, 'user balance retrieved', user_wallet)

    except Exception as e:
        return ResponseService.send_response(response, status.HTTP_400_BAD_REQUEST,
                                             f'Error in getting user wallet: {str(e)}')
