from typing import Optional, Union

from bson import ObjectId

from apps.user.interfaces.user_interface import UserOut
from core.depends.get_object_id import PyObjectId
from pydantic import Field, BaseModel

from core.depends.model import SBaseModel

from core.utils.loggly import logger


class WalletIn(BaseModel):
    user: PyObjectId


class WalletOut(BaseModel):
    id: Optional[PyObjectId]
    user: PyObjectId
    balance: int

    def __init__(self, **pydict):
        try:
            super().__init__(**pydict)
            self.id = pydict.pop('_id')
        except Exception as e:
            logger.error(f'Error while mapping the id to record - {e}')

    class Config:
        arbitrary_types_allowed = True


class Wallet(WalletIn, SBaseModel):
    __name__ = 'wallets'
    id: Optional[PyObjectId] = Field(alias='_id')
    user: Union[PyObjectId, UserOut]
    balance: int

    def __init__(self, **pydict):
        try:
            super().__init__(**pydict)
            self.id = pydict.pop('_id')
        except Exception as e:
            logger.error(f'Error while mapping the id to record - {e}')

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        json_encoders = {
            ObjectId: str
        }
