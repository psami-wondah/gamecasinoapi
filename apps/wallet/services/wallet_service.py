import threading
from datetime import datetime
from typing import Any

from pymongo.client_session import ClientSession

from apps.user.interfaces.user_interface import User
from apps.wallet.interfaces.wallet_interface import Wallet, WalletOut
from apps.wallettransaction.interfaces.wallettransaction_interface import WalletAction, WalletTransaction

from core.db import db, client
from core.depends import PyObjectId
from core.utils.model_utility_service import ModelUtilityService

modelUtilityService = ModelUtilityService


class WalletService:

    @staticmethod
    def update_wallet_balance(wallet_id, amount, session: ClientSession = None) -> None:
        db.wallets.update_one({'_id': wallet_id}, {'$inc': {
            'balance': amount}, '$set': {'updatedAt': datetime.now()}}, session=session)

    @staticmethod
    def create_wallet(user: User, session: ClientSession) -> None:
        dict_wallet = {
            'user': user.id,
            'balance': 0
        }
        modelUtilityService.model_create(
            db.wallets, dict_wallet, Wallet, session)

    @staticmethod
    def retrieve_wallet(user: User) -> WalletOut:
        db_resp = db.wallets.find_one({'user': user.id, 'isDeleted': False})
        if db_resp is None:
            raise Exception('user wallet not found')
        return WalletOut(**db_resp)

    @staticmethod
    def get_user_wallet(user_id: PyObjectId) -> Wallet:
        db_resp = db.wallets.find_one({'user': user_id, 'isDeleted': False})
        if db_resp is None:
            raise Exception('user wallet not found')
        return Wallet(**db_resp)

    @staticmethod
    def debit_wallet(user: User, amount: int, ref: str, session: ClientSession = None) -> None:
        mutex = threading.Lock()

        mutex.acquire()
        try:
            user_wallet = WalletService.get_user_wallet(user.id)

            if user_wallet.balance < amount:
                raise Exception('wallet balance insufficient')

            wallet_transaction_data = {
                'wallet': user_wallet.id,
                'user': user.id,
                'action': WalletAction.debit.value,
                'amount': amount,
                'previousBalance': user_wallet.balance,
                'ref': ref
            }
            WalletService.update_wallet_balance(
                user_wallet.id, -amount, session)
            modelUtilityService.model_create(db.wallettransactions,
                                             wallet_transaction_data, WalletTransaction, session)
        except Exception as e:
            raise e
        finally:
            mutex.release()

    @staticmethod
    def credit_wallet(user: User, amount: int, ref: str, paymentMethod: str, metaData: Any) -> None:
        mutex = threading.Lock()

        mutex.acquire()

        session = client.start_session()
        session.start_transaction()
        try:
            user_wallet = WalletService.get_user_wallet(user.id)
            wallet_transaction_data = {
                'wallet': user_wallet.id,
                'user': user.id,
                'action': WalletAction.credit.value,
                'amount': amount,
                'previousBalance': user_wallet.balance,
                'ref': ref,
                'metaData': metaData,
                'paymentMethod': paymentMethod,
            }
            WalletService.update_wallet_balance(
                user_wallet.id, amount, session)
            modelUtilityService.model_create(db.wallettransactions,
                                             wallet_transaction_data, WalletTransaction, session)
            session.commit_transaction()
        except Exception as e:
            # operation exception, interrupt transaction
            session.abort_transaction()
            raise e
        finally:
            session.end_session()
            mutex.release()
