from pathlib import Path

import emails as emails
from emails.template import JinjaTemplate

from apps.user.interfaces.user_interface import AbstractUser, User
from core.utils.helper_service import HelperService
from core.config import settings

helperService = HelperService


class EmailService:

    @staticmethod
    def send_template_email(email_to: str, subject_template: str = '',
                            html_template: str = '', environment=None,
                            ) -> None:
        if environment is None:
            environment = {}
        assert settings.EMAILS_ENABLED, 'Email is Disabled'
        message = emails.Message(
            subject=JinjaTemplate(subject_template),
            html=JinjaTemplate(html_template),
            mail_from=(settings.EMAILS_FROM_NAME, settings.MAIL_SENDER),
        )
        smtp_options = {'host': settings.MAIL_SERVER, 'port': settings.MAIL_PORT,
                        'tls': settings.MAIL_TLS, 'user': settings.MAIL_USERNAME, 'password': settings.MAIL_PASSWORD}

        message.send(to=email_to, render=environment, smtp=smtp_options)

    @staticmethod
    def send_verification(user: AbstractUser) -> None:
        token = helperService.generate_confirmation_token(user.email)
        confirm_url = settings.UI_URL + 'account/confirm/' + token

        subject = 'Verify your email address'
        with open(Path(settings.EMAIL_TEMPLATES_DIR) / 'verify_account.html') as f:
            template_str = f.read()

        EmailService.send_template_email(
            email_to=user.email,
            subject_template=subject,
            html_template=template_str,
            environment={
                'name': f'{user.firstName} {user.lastName}',
                'link': confirm_url,
                'valid_hours': settings.TOKEN_EXPIRATION_IN_HR,
            },
        )

    @staticmethod
    def send_forgotten_password_link(user: User) -> None:
        token = helperService.generate_confirmation_token(user.email)
        password_reset_url = settings.UI_URL + 'account/update-password/' + token

        subject = 'Forgotten your password?'
        with open(Path(settings.EMAIL_TEMPLATES_DIR) / 'resetpassword.html') as f:
            template_str = f.read()

        EmailService.send_template_email(
            email_to=user.email,
            subject_template=subject,
            html_template=template_str,
            environment={
                'name': f'{user.firstName} {user.lastName}',
                'link': password_reset_url,
                'valid_hours': settings.TOKEN_EXPIRATION_IN_HR,
            },
        )
