from typing import Any

import requests
import json

from core.config import settings
from core.utils.loggly import logger


class SlackService:

    @staticmethod
    def send_message(text: str, channel: str, attachments: Any = None) -> None:
        try:
            body = {
                'text': text,
                'channel': channel,
                'username': 'jevanbot'
            }

            if attachments is not None:
                body['attachments'] = attachments

            requests.post(settings.SLACK_HOOK, data=json.dumps(body))
        except Exception as e:
            logger.info(f'Error sending message to slack - {e}')

    @staticmethod
    def notify_slack_of_demigod() -> None:
        text = f'>*🪐 Demigod Reminder 🪐* \n `This is just a daily bot reminder that my baby Evans is a fuckin ' \
               f'godd!!!!!` '

        SlackService.send_message(text, 'general')
