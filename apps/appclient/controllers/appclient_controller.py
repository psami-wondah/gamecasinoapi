# -*- coding: utf-8 -*-
from apps.appclient.interfaces.appclient_interface import AppClientIn
from apps.appclient.services.appclient_service import AppClientService

from fastapi import status, Request, Response
from fastapi.routing import APIRouter

from core.utils.response_service import ResponseService

appClientService = AppClientService

router = APIRouter(prefix='/api/v1/appclient',
                   tags=['App Client 🌈'])


@router.post('/create')
async def create_app_client(request: Request, response: Response, app_client: AppClientIn):
    try:
        request.app.logger.info(
            f'')
        client_obj = appClientService.create_client(app_client)
        request.app.logger.info(f'')
        return ResponseService.send_response(response, status.HTTP_200_OK, 'app client created', client_obj)

    except Exception as e:
        request.app.logger.error(
            f' - {str(e)}')
        return ResponseService.send_response(response, status.HTTP_400_BAD_REQUEST,
                                             f'Error in creating app client: {str(e)}')
