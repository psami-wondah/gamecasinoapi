from functools import wraps

from fastapi import Request, HTTPException

from apps.appclient.interfaces.appclient_interface import AppClient, AppClientIn
from core.db import db
from core.utils.helper_service import HelperService
from core.utils.model_utility_service import ModelUtilityService

modelUtilityService = ModelUtilityService
helperService = HelperService


class AppClientService:
    @staticmethod
    def clientAuth(func):
        @wraps(func)
        async def wrapper(request: Request, *args, **kwargs):
            client_id = request.headers.get('clientID')
            client_secret = request.headers.get('clientSecret')

            if client_id is None or client_secret is None:
                raise HTTPException(status_code=403, detail='incomplete client credentials specified')

            app_client = AppClientService.get_client_by_id(client_id)

            if app_client is False or app_client.clientSecret != client_secret:
                raise HTTPException(status_code=403, detail='incomplete client credentials specified')

            request.state.app_client = app_client
            return func(request, *args, **kwargs)

        return wrapper

    @staticmethod
    def get_client_by_id(client_id: str) -> AppClient:
        app_client = db.appclients.find_one({
            'clientID': client_id
        })

        if app_client is None:
            raise HTTPException(status_code=403, detail='client not found')

        return AppClient(**app_client)

    @staticmethod
    def get_client_by_secret(client_secret: str) -> AppClient:
        app_client = db.appclients.find_one({
            'clientSecret': client_secret
        })

        if app_client is None:
            raise HTTPException(status_code=403, detail='client not found')

        return AppClient(**app_client)

    @staticmethod
    def create_client(client: AppClientIn) -> AppClient:
        client.clientSecret = helperService.generate_random(24)
        client.clientID = helperService.generate_random(12)
        dict_client = client.dict(by_alias=True)

        client_obj = modelUtilityService.model_create(db.appclients, dict_client, AppClient)

        return client_obj
