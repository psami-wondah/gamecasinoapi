from typing import Optional

from bson import ObjectId
from pymongo import ASCENDING

from core.db import db
from core.depends.get_object_id import PyObjectId
from pydantic import Field, BaseModel

from core.depends.model import SBaseModel

from core.utils.loggly import logger


class AppClientAuth(BaseModel):
    clientID: Optional[str]
    clientSecret: Optional[str]


class AppClientIn(AppClientAuth):
    name: str
    callBackUrl: Optional[str]
    appUrl: Optional[str]

    class Config:
        schema_extra = {
            'example': {
                'name': 'demi-chee',
                'callBackUrl': 'demigod.com/callback',
                'appUrl': 'demigod.com',
            }
        }


class AppClientOut(SBaseModel):
    id: Optional[PyObjectId]
    name: str
    clientID: str
    clientSecret: str
    callBackUrl: Optional[str]
    appUrl: Optional[str]

    def __init__(self, **pydict):
        try:
            super().__init__(**pydict)
            self.id = pydict.pop('_id')
        except Exception as e:
            logger.error(f'Error while mapping the id to record - {e}')

    class Config:
        arbitrary_types_allowed = True


class AppClient(SBaseModel):
    __name__ = 'appclients'
    id: Optional[PyObjectId] = Field(alias='_id')
    name: str
    clientID: str
    clientSecret: str
    callBackUrl: str
    appUrl: str

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        json_encoders = {
            ObjectId: str
        }

    @staticmethod
    def init():
        db.country.create_index(
            [('name', ASCENDING)], unique=True)
