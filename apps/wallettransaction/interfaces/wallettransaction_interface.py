from enum import Enum

from pymongo import ASCENDING

from typing import Optional, Union

from bson import ObjectId

from apps.user.interfaces.user_interface import UserOut
from core.depends.get_object_id import PyObjectId
from pydantic import Field, BaseModel

from core.depends.model import SBaseModel
from core.db import db


class PaymentMethod(str, Enum):
    internalFund = 'internalFund'


class WalletAction(str, Enum):
    credit = 'credit'
    debit = 'debit'


class WalletTransactionIn(BaseModel):
    wallet: PyObjectId
    user: PyObjectId
    action: WalletAction
    previousBalance: int
    amount: int
    ref: str
    paymentMethod: Optional[PaymentMethod]
    metaData: Optional[dict]


class WalletTransaction(WalletTransactionIn, SBaseModel):
    __name__ = 'wallettransactions'
    id: Optional[PyObjectId] = Field(alias='_id')
    wallet: PyObjectId
    user: Union[PyObjectId, UserOut]
    amount: int
    action: WalletAction
    previousBalance: int
    ref: str
    paymentMethod: PaymentMethod
    metaData: dict

    class Config:
        allow_population_by_alias = True
        arbitrary_types_allowed = True
        json_encoders = {
            ObjectId: str
        }

    @staticmethod
    def init():
        db.wallettransactions.create_index(
            [('ref', ASCENDING)], unique=True)
