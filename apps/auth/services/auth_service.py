from typing import Union

from apps.user.interfaces.user_interface import AbstractUser, UserLoginInput, User
from apps.user.services.user_service import UserService
from core.utils.helper_service import HelperService
from apps.auth.services.jwt_service import JWTService

userService = UserService
helperService = HelperService
jwtService = JWTService


class AuthService:

    @staticmethod
    def create_account(user: AbstractUser) -> dict[str, User]:
        user.password = helperService.hash_password(user.password)
        user_created = userService.create_user(user)
        return {'user': user_created}

    @staticmethod
    def login_user(user: UserLoginInput) -> dict[str, Union[User, str]]:
        user_logged_in = userService.login_user(user)
        token = jwtService.sign_jwt(str(user_logged_in.id))
        return {'user': user_logged_in, 'token': token}
