from fastapi import Request, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from jwt import DecodeError

from apps.auth.services.jwt_service import JWTService
from apps.user.services.user_service import UserService
from core.utils.response_service import ResponseService

jwtService = JWTService
responseService = ResponseService
userService = UserService


class JWTBearer(HTTPBearer):
    def __init__(self, auto_error: bool = True):
        super(JWTBearer, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        try:
            credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
            if credentials:
                if not credentials.scheme == "Bearer":
                    raise HTTPException(
                        status_code=403, detail="Invalid authentication scheme.")
                else:
                    payload = self.verify_jwt(credentials.credentials)
                    if not payload:
                        raise HTTPException(
                            status_code=403, detail="Invalid token or expired token.")
                    request.state.user = userService.get_user_by_id(
                        payload['user_id'])
                    return credentials.credentials
            else:
                raise HTTPException(
                    status_code=403, detail="Invalid authorization code.")
        except DecodeError:
            raise HTTPException(
                status_code=403, detail="Invalid authorization code.")

        except Exception:
            raise HTTPException(
                status_code=401, detail=responseService.status_code_message[401])

    @staticmethod
    def verify_jwt(jwt_token: str) -> dict:
        payload = jwtService.decode_jwt(jwt_token)

        return payload
