from google.oauth2 import id_token
from google.auth.transport import requests

from apps.user.interfaces.user_interface import AbstractUser, User, SignUpMethod
from apps.user.services.user_service import UserService
from core.utils.helper_service import HelperService
from core.config import settings
from operator import itemgetter

userService = UserService
helperService = HelperService


class GoogleService:

    @staticmethod
    async def verify_oauth_sign_in(user_id_token: str) -> dict[str, User]:
        try:
            idinfo = id_token.verify_oauth2_token(
                user_id_token, requests.Request())
            aud, email, name = itemgetter('aud', 'email', 'name')(idinfo)
            if aud not in [settings.GOOGLE_CLIENT_ID]:
                raise ValueError('Could not verify audience.')

            # ID token is valid. Get the user's Google Account ID from the decoded token.
            # userid = idinfo.sub
            try:
                user = userService.get_user_by_email(email)
            except ValueError:
                first_name, other_name = name.split()
                password = helperService.hash_password('password')
                user_data = {
                    'firstName': first_name,
                    'lastName': other_name,
                    'email': email,
                    'password': password,
                    'isVerified': True,
                    'signUpMethod': SignUpMethod.google.value
                }
                user_obj = AbstractUser(**user_data)
                user = userService.create_user(user_obj)

            return {'user': user}
        except Exception as e:
            # Invalid token
            print('invalid token', e)
