from typing import Any

from bson import ObjectId
from pydantic import EmailStr

from apps.featureconfig.interfaces.featureconfig_interface import FeatureName
from apps.featureconfig.services.featureconfig_service import FeatureConfigService
from apps.notification.slack.services.slack_service import SlackService
from apps.user.interfaces.user_interface import User, AbstractUser, UserLoginInput, UserResetPasswordInput, SignUpMethod

from apps.wallet.services.wallet_service import WalletService
from apps.wallettransaction.interfaces.wallettransaction_interface import PaymentMethod
from core import constants
from core.db import db, client
from core.depends import PyObjectId
from core.utils.helper_service import HelperService
from core.utils.model_utility_service import ModelUtilityService

walletService = WalletService
slackService = SlackService
modelUtilityService = ModelUtilityService
helperService = HelperService
featureConfigService = FeatureConfigService


class UserService:

    @staticmethod
    def create_user(user: AbstractUser) -> User:
        session = client.start_session()
        session.start_transaction()
        try:
            UserService.check_if_username_exist_and_fail(user.username)
            user.signUpMethod = SignUpMethod.email.value
            dict_user = user.dict(by_alias=True)
            user_obj = modelUtilityService.model_create(
                db.users, dict_user, User, session)
            walletService.create_wallet(user_obj, session)
            session.commit_transaction()
            try:
                if featureConfigService.is_feature_enabled(FeatureName.SignUpBonus.value):
                    walletService.credit_wallet(user_obj, constants.SIGNUP_BONUS_AMOUNT,
                                                f'signup-bonus-{user_obj.id}', PaymentMethod.internalFund.value,
                                                {'type': 'signup-bonus-transaction'})
            except Exception as e:
                slackService.send_message(
                    f'Error crediting wallet with bonus after sign up \n userID: {user_obj.id} \n error: {e}',
                    'error-report')
            return user_obj
        except Exception as e:
            session.abort_transaction()
            raise e
        finally:
            session.end_session()

    @staticmethod
    def login_user(login_user_input: UserLoginInput) -> User:
        user_obj = UserService.get_user_by_email(login_user_input.email)

        if not helperService.verify_password(user_obj.password, login_user_input.password):
            raise Exception('wrong credentials')
        if user_obj.isVerified is False:
            raise Exception('user not verified')
        return user_obj

    @staticmethod
    def get_user_by_id(_id: PyObjectId) -> User:
        db_resp = db.users.find_one({'_id': ObjectId(_id), 'isDeleted': False})
        if db_resp is None:
            raise ValueError('user not found')
        return User(**db_resp)

    @staticmethod
    def get_user_by_email(email: EmailStr) -> User:
        db_resp = db.users.find_one({'email': email, 'isDeleted': False})
        if db_resp is None:
            raise ValueError('user not found')
        return User(**db_resp)

    @staticmethod
    def get_user_by_username(username: str) -> User:
        db_resp = db.users.find_one({'username': username, 'isDeleted': False})
        if db_resp is None:
            raise ValueError('user not found')
        return User(**db_resp)

    @staticmethod
    def check_if_username_exist_and_fail(username: str) -> Any:
        db_resp = db.users.find_one({'username': username, 'isDeleted': False})
        if db_resp is None:
            return True
        raise ValueError('username already exist')

    @staticmethod
    def update_user(email: EmailStr, password_reset_dto: UserResetPasswordInput) -> None:
        user = UserService.get_user_by_email(email)

        new_password = helperService.hash_password(password_reset_dto.password)

        query = {'_id': user.id, 'isDeleted': False}
        record_to_update = {'password': new_password}

        modelUtilityService.model_update(db.users, query, record_to_update)

    @staticmethod
    def verify_email(email: EmailStr) -> None:
        user = UserService.get_user_by_email(email)

        if not user.isVerified:
            query = {'email': email, 'isDeleted': False}
            record_to_update = {'isVerified': True}

            modelUtilityService.model_update(db.users, query, record_to_update)
