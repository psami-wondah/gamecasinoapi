from enum import Enum
from typing import Optional, Any, Union

from bson import ObjectId

from apps.country.interfaces.country_interface import CountryOut
from core.db import db
from core.depends.get_object_id import PyObjectId
from pydantic import BaseModel, Field, EmailStr
from pymongo import ASCENDING

from core.depends.model import SBaseModel
from core.utils.loggly import logger


class SignUpMethod(str, Enum):
    google = 'google-oauth'
    email = 'email-signup'


class UserLoginInput(BaseModel):
    password: str
    email: Optional[EmailStr]

    def __init__(self, email, **data: Any):
        super().__init__(**data)
        self.email = email.lower()

    class Config:
        schema_extra = {
            'example': {
                'email': 'evans@demigod.com',
                'password': 'password'
            }
        }


class UserResetPasswordInput(BaseModel):
    password: str


class AbstractUser(UserLoginInput):
    firstName: str
    lastName: str
    isVerified: bool = Field(default=False)
    username: str
    country: Optional[Union[CountryOut, PyObjectId]]
    signUpMethod: Optional[SignUpMethod]

    class Config:
        schema_extra = {
            'example': {
                'firstName': 'evans',
                'lastName': 'demigod',
                'email': 'evans@demigod.com',
                'password': 'password',
                'username': 'sss',
                'country': '61689fc4dc4f8ba4c07f52e2'
            }
        }


class UserOut(AbstractUser, SBaseModel):
    id: Optional[PyObjectId]

    def __init__(self, **pydict):
        try:
            super().__init__(**pydict)
            self.id = pydict.pop('_id')
        except Exception as e:
            logger.error(f'Error while mapping the id to record - {e}')

    class Config:
        schema_extra = {
            'example': {
                'name': 'evans',
                'email': 'evans@demigod.com',
            }
        }


class User(AbstractUser, SBaseModel):
    __name__ = 'users'
    id: Optional[PyObjectId] = Field(alias='_id')
    firstName: str
    lastName: str
    email: Optional[EmailStr]
    username: Optional[str]
    password: str
    isVerified: bool = Field(default=False)
    country: Optional[Union[PyObjectId, CountryOut]]
    signUpMethod: SignUpMethod

    def __init__(self, **pydict):
        try:
            super().__init__(**pydict)
            self.id = pydict.pop('_id')
        except Exception as e:
            logger.error(f'Error while mapping the id to record - {e}')

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {
            ObjectId: str
        }

    @staticmethod
    def init():
        db.users.create_index(
            [('email', ASCENDING)], unique=True)
